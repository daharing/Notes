#!/usr/bin/env python3

# Original source from pdudaemon
# https://github.com/pdudaemon/pdudaemon/blob/main/pdudaemon/drivers/tplink.py

#  Confirmed devices that work with this driver
#    - KP303: https://www.tp-link.com/uk/home-networking/smart-plug/kp303/
#    - KP105: https://www.tp-link.com/uk/home-networking/smart-plug/kp105/
#    - HS105: https://www.tp-link.com/us/home-networking/smart-plug/hs105/

import json
import socket
from struct import pack
import os
from enum import Enum

class Command(Enum):
    OFF = 0
    ON = 1

class TPLink:

    def __init__(self, hostname):
        self.hostname = hostname
        self.childinfo = {}
        self.getinfo()

    def encrypt(self, string):
        key = 171
        result = pack(">I", len(string))
        for i in string:
            a = key ^ ord(i)
            key = a
            result += bytes([a])
        return result

    def decrypt(self, string):
        key = 171
        result = ""
        for i in string:
            a = key ^ i
            key = i
            result += chr(a)
        return result


    def send_command(self, json_string):
        try:
            sock_tcp = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            sock_tcp.settimeout(10)
            sock_tcp.connect((self.hostname, 9999))
            sock_tcp.settimeout(10)
            sock_tcp.send(self.encrypt(json_string))
            data = sock_tcp.recv(2048)
            sock_tcp.close()
            decrypted = self.decrypt(data[4:])
            return (decrypted)
        except socket.error:
            print("Could not connect to host {self.hostname}:9999")

    def getinfo(self):
        print("getinfo(): Requesting info from tplink smart outlet!")
        datadict = {
            'system': {
                'get_sysinfo': {
                }
            }
        }
        res = self.send_command(json.dumps(datadict))
        if res:
            print(f"getinfo(): res == {res}")
            resdict = json.loads(res)
            print(f"getinfo(): resdict == {resdict}")
            self.childinfo = resdict.get("system", {}).get("get_sysinfo", {}).get("children", {})
            print(f"getinfo(): childinfo == {self.childinfo}")

    def get_context(self, port_number):
        print("get_context():")
        for child in self.childinfo:
            print("   child == ")
            print(child)
            print("   child['alias'] == ")
            print(child['alias'])
            print("   child['alias'].split(\"_\") == ")
            print(child['alias'].split("_"))
            child_port = (int(child['alias'].split("_")[-1]) + 1)
            print("   child_port == ")
            print(child_port)
            if int(port_number) == int(child_port):
                print("   if is true")
                print({"child_ids": [child["id"]]})
                print("get_context() -- END")
                return ({"child_ids": [child["id"]]})
        print("get_context() -- END")
        return ({})

    def port_interaction(self, command, port_number):
        state = 0
        context = None

        if command == Command.ON:
            state = 1
        else:
            state = 0

        if self.childinfo:
            if int(port_number) > len(self.childinfo):
                return (False)
            context = self.get_context(port_number)

        datadict = {
            'context': context,
            'system': {
                'set_relay_state': {
                    'state': state,
                }
            }
        }

        print(datadict)
        res = self.send_command(json.dumps(datadict))
        return bool(res)

if __name__ == "__main__":

    # IP of the tplink smart outlet device
    ip = "50.0.0.172"
    # Which plug/outlet to turn on/off
    outlet_number = "1"
    # Command to turn outlet/plug on/off
    command = Command.ON

    plug = TPLink(ip)
    plug.port_interaction(command, outlet_number)
