# Instructions
## Relevant Files
* [tplink-smart-outlets.md](tplink-smart-outlets.md) (this README)
* [tplink-smart-outlets.py](tplink-smart-outlets.py)
## Set Up
1. Need `python3` installed
1. Connect tplink smart outlet to WiFi network
1. Connect computer to same network
## Configure
1. Edit tplink-smart-outlets.py:
    * In section `if __name__ == "__main__":`
        * Set `ip` variable to ip of smart outlet
        * Set `outlet_number` to which outlet/plug on device to control (numbering starts at `1` and value does not matter for single-outlet smart plugs)
        * Set `command` to `Command.ON` or `Command.OFF` to turn outlet/plug on/off
## Execute/Run
1. `./tplink-smart-outlets.py` or `python3 tplink-smart-outlets.py`
## Results
1. Outlet will turn specified plug/outlet on/off