# Raspberry Pi
## Set Up
### Download & Flash
1. Download latest image from [here](https://www.raspberrypi.com/software/operating-systems/): ie `wget https://downloads.raspberrypi.org/raspios_lite_armhf/images/raspios_lite_armhf-2022-04-07/2022-04-04-raspios-bullseye-armhf-lite.img.xz`
1. Extract archive: `xz --decompress *raspio*.img.xz` (or `unxz *raspio*.img.xz`)
1. Determine which device is the sd card to flash: `lsblk`
1. Flash SD card: `sudo dd if=*raspio*.img of=/dev/<sdcard> bs=1M conv=fsync`
### Headless Set Up
1. Mount `boot` partition & open a terminal there
1. Enable SSH: `touch ssh`
1. For new `Raspberry Pi OS`, create a username and password:
    * `echo -n "<username>:" > userconf`
    * `echo "<password>" | openssl passwd -6 -stdin >> userconf`
1. [Enable UART](https://tp4348.medium.com/serial-to-raspberry-pi-da635122b4d0)
    * `echo "enable_uart=1" >> config.txt`
    * To enable kernel console logs during boot: `sed -e 's/quiet//g' -e 's/plymouth.ignore-serial-consoles//g' -i cmdline.txt`
    * [RPi UART pins](https://pinout.xyz/pinout/uart):
        * GND == pin 6
        * TX  == pin 8
        * RX  == pin 10
1. Enable Wireless/Connect to WiFi Network  
    Fill in your country code, network SSID, and network password and run the command:
    ```bash
    echo 'country=US
    ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
    update_config=1

    network={
    scan_ssid=1
    ssid="your_wifi_ssid"
    psk="your_wifi_password"
    proto=RSN
    key_mgmt=WPA-PSK
    pairwise=CCMP
    auth_alg=OPEN
    }' > wpa_supplicant.conf
    ```
    
1. Mount rootfs partition & open a terminal there
1. Ethernet Set Up
    * No Set Up Required for RPi's with a physical RJ45
        * Set Static IP
            * `sudo echo 'interface eth0' >> /etc/dhcpcd.conf`
            * `sudo echo 'static ip_address=<ip_addr>/24' >> /etc/dhcpcd.conf`
            * `sudo echo 'static routers=<router_ip>' >> /etc/dhcpcd.conf`
            * DNS ip is often the same as the router ip: `sudo echo 'static domain_name_servers=<dns_ip>' >> /etc/dhcpcd.conf`
    * TODO; RPi Zero's
1. Set Hostname: `sudo echo "hostname" > etc/hostname` and `sudo sed -e 's/raspberrypi/hostname/g' -i etc/hosts`
### Before First Login
Add your public ssh key to RPi to enable password-less login (use `ssh-keygen` if you don't have one)

* `ssh-copy-id user@hostname.local`

### First Login
#### Set Up SSH
Note: section Before First Login should have allowed you to login with a password now

* Set SSH Port: `echo "Port 22" >> /etc/ssh/sshd_config`
* Enable ssh key auth: `echo "PubkeyAuthentication yes" >> /etc/ssh/sshd_config`
* Disable password logins: `echo "PasswordAuthentication no" >> /etc/ssh/sshd_config`
* Disallow root login: `echo "PermitRootLogin no" >> /etc/ssh/sshd_config`
#### Update
* `sudo apt update && sudo apt upgrade -y && sudo apt autoclean && sudo apt autoremove`
* Install Basics: `sudo apt install build-essential`
### Reboot
* `sudo reboot`