## Notes
* `*.rs` is the rust file extension for code files
* `rustc` is the rust compiler
* `cargo` is rust's build system, package manager, and dependency handler. Does things like download & build dependencies (libraries) and build the project code
* Rust requires a linker be installed (ie like the C linker in `gcc`)
* Some rust packages (ie crates) require C compiler be installed (ie `gcc`)
* Dependencies are libraries!
## Installation
* `curl --proto '=https' --tlsv1.2 https://sh.rustup.rs -sSf | sh`
## Commands
| Command | Description |
| :--- | :--- |
| **rustup** |  |
| `rustup doc` | View rust documentation (installed locally) in browser. This includes rust examples and *The Rust Programming Language* book |
| `rustup component add rustfmt` | Install the rust code formatter. Use the `rustfmt` and `cargo fix` commands |
| `rustup component add clippy` | Install rust lint analyzers. These tools help improve your code by catching common mistakes. Use `cargo clippy`  |
| `rustup update` | Update rust |
| `rustup self uninstall` | Uninstall rust |
| **rustc** |  |
| `rustc <file.rs>` | `rustc` is the rust compiler. This will compile the rust file |
| `rustfmt` | Re-format/style code according to the rust community style. This provides a common and consistent coding style (does not change semantics) |
| **cargo** |  |
| `cargo new <proj_name>` | Create cargo project. `Cargo.toml` is the config file (Tom's Obvious, Minimal Language format). Creates git repo. Creates `src/` directory with hello world code. |
| `cargo build` | Build cargo project. Resulting binary is in `target/debug/<proj_name>` |
| `cargo build --release` | Build cargo project for release. Build with optimizations. Resulting binary is in `target/release/<proj_name>` |
| `cargo run` | Build & execute project (build source code and execute resulting binary) |
| `cargo check` | Build code without producing a binary. Much faster than building code. |
| `cargo doc --open` | Create and open local docs for project dependencies |
| `cargo fmt` | Re-format/style code according to the rust community style. This provides a common and consistent coding style (does not change semantics) |
| `cargo fix` | This command is able to 'fix' some compilation errors |
| `cargo clippy` | Find common mistakes and ways to improve your code |
|  |  |

## Language
* Associated functions (Rust) == static functions (C)
